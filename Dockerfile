FROM microsoft/dotnet:2.2-aspnetcore-runtime

WORKDIR /app

COPY publish/ ./

CMD export ASPNETCORE_URLS=http://*:$PORT && dotnet SampleWebApi.dll
