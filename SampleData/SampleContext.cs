using Microsoft.EntityFrameworkCore;
using MySql.Data.EntityFrameworkCore.Extensions;
using SampleData.Entities;

namespace SampleData {
    public class SampleContext : DbContext {
        public DbSet<Toon> Toons { get; set; }

        protected override void OnConfiguring (DbContextOptionsBuilder optionsBuilder) {
            optionsBuilder.UseMySQL ("server=localhost;database=testbd;user=ldumer;password=781245");
        }

        protected override void OnModelCreating (ModelBuilder modelBuilder) {
            base.OnModelCreating (modelBuilder);

            modelBuilder.Entity<Toon> (entity => {
                entity.HasKey (e => e.Id);
                entity.Property (e => e.Name).IsRequired ();
            });
        }
    }
}